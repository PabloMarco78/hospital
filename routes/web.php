<?php

use App\Http\Controllers\DoctorController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\QuestionnaireController;
use App\Http\Controllers\HomeController;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/home', function () {
    return view('home');
})->middleware('auth');

//Route::get('/home', [HomeController::class, 'index']);

Auth::routes();
Route::resource('doctors', DoctorController::class)->middleware('auth');
Route::resource('patients', PatientController::class)->middleware('auth');
Route::resource('users', UserController::class)->middleware('auth');
Route::resource('surveys', SurveyController::class)->middleware('auth');
Route::resource('roles', RoleController::class)->middleware('auth');
Route::resource('questions', QuestionController::class);
Route::resource('answers', AnswerController::class);

Route::get('/surveys/{survey}/questions/create', [QuestionController::class, 'create']);

Route::post('/surveys/{survey}/questions', [QuestionController::class, 'store']);

Route::delete('/surveys/{survey}/questions/{question}', [QuestionController::class, 'destroy']);

Route::get('/questionnaires/{survey}-{slug}', [QuestionnaireController::class, 'show']);

Route::post('/questionnaires/{survey}-{slug}', [QuestionnaireController::class, 'store']);


Route::get('file-import-export', [UserController::class, 'fileImportExport']);
Route::post('file-import', [UserController::class, 'fileImport'])->name('file-import');
Route::get('file-export', [UserController::class, 'fileExport'])->name('file-export');

//exportar en excel
//Route::get('/questionnaires/{survey}-{slug}', [QuestionnaireController::class, 'export']);

//Route::get('users/export/', [UserController::class, 'export']);

// export routes
Route::get("users", [UserController::class, "index"]);
Route::get('export', [UserController::class, "export"])->name("export");

Route::get("questionnaires", [QuestionnaireController::class, "index"]);
Route::get('export', [QuestionnaireController::class, "export"])->name("export");









Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/
