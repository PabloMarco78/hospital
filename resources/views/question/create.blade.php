@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center ">
        <div class="col-md-12">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
                
                <div class="class-header m-5">
                    <h2 class="text-center"><i class="fas fa-question"> Añadir una nueva pregunta </i></h2>
                </div>
            </div>

                    <div class="card-body">

                        <form action="/surveys/{{ $survey->id }}/questions" method="post">
                        @csrf

                                <div class="form-group">
                                    <label for="question"> <strong>Pregunta</strong> </label>
                                    <!-- tabla question tiene una columna llamada question-->
                                    <input type="text" name="question[question]" class="form-control" 
                                    value="{{ old('question.question') }}"
                                    id="question" aria-describedby="questionHelp" placeholder="Introduce una pregunta">
                                    <small id="question" class="form-text text-muted">Introduce tu pregunta aquí </small>
                                    @error('question.question')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <fieldset>
                                        <legend> <strong>Opciones</strong></legend>
                                        <small id="OpcionesHelp" class="form-text text-muted">Opciones disponibles </small>

                                        <div>
                                            <div class="form-group">
                                                <label for="answer1"><strong>Opción 1</strong>  </label>
                                                <!-- tabla answer tiene una columna llamada answer-->
                                                <input type="text" name="answers[][answer]" class="form-control" id="answer1" aria-describedby="H" 
                                                value="{{ old('answers.0.answer') }}"
                                                placeholder="Introduce la opción 1">

                                                @error('answers.0.answer')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div>
                                            <div class="form-group">
                                                <label for="answer2"><strong>Opción 2</strong> </label>
                                                <input type="text" name="answers[][answer]" class="form-control" id="answer2" aria-describedby="H" 
                                                value="{{ old('answers.1.answer') }}"
                                                placeholder="Introduce la opción 2">

                                                @error('answers.1.answer')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div>
                                            <div class="form-group">
                                                <label for="answer3"><strong>Opción 3</strong> </label>
                                                <input type="text" name="answers[][answer]" class="form-control" id="answer3" aria-describedby="H" 
                                                value="{{ old('answers.2.answer') }}"
                                                placeholder="Introduce la opción 3">

                                                @error('answers.2.answer')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div>
                                            <div class="form-group">
                                                <label for="answer4"><strong>Opción 4</strong> </label>
                                                <input type="text" name="answers[][answer]" class="form-control" id="answer4" aria-describedby="OpcionesHelp" 
                                                value="{{ old('answers.3.answer') }}"
                                                placeholder="Introduce la opción 4">

                                                @error('answers.3.answer')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                    </fieldset>

                            <button type="submit" class="btn text-white"style="background:#e58c8a;">Añadir Pregunta</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection