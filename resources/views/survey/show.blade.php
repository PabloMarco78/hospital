@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card mt-5">
                <div class="card-header"> <h5><strong> Cuestionario nº: </strong> {{$survey->id}}</h5></div>
                <div class="card-header"> <h5><strong> Título: </strong> {{$survey->title}}</h5></div>
                <div class="card-header"> <h5><strong> Descripción: </strong> {{$survey->description}}</h5></div>
                @can('createQuestion', $survey)
                <div class="card-body">

                <!-- nested relationship: Un cuestionario tiene preguntas-->
                    <a class="btn text-white" style="background:#e58c8a;" href="/surveys/{{$survey->id}}/questions/create">Añadir Pregunta</a>
                </div>
                @endcan
            </div>
            <div class="card-header">  <h5><strong>El cuestionario {{$survey->title}} tiene las siguientes preguntas :</strong></h5> </div>
            <!-- un foreach que recorre las preguntas del cuestionario gracias al lazy loading-->
            @foreach($survey->questions as $key => $question)

            <div class="card mt-4">
                <div class="card-header"> <strong> Pregunta nº {{$key+1}} :</strong> {{$question->question}}</div>

                <div class="card-body">
                    <ul class="list-group">

                        @foreach($question->answers as $key=>$answer)

                        <li class="list-group-item"><strong> {{$key+1}} </strong> {{ $answer->answer }}</li>
                        @endforeach

                    </ul>
                </div>
                @can('deleteQuestion', $survey)
                <div class="card-footer">

                    <form action="/surveys/{{$survey->id}}/questions/{{$question->id}}" method="post">
                        @method('DELETE')

                        @csrf

                        <button type="submit" class="btn btn-sm btn-outline-danger">Borrar Pregunta</button>



                    </form>

                </div>
                @endcan

            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection