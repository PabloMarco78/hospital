@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row justify-content-center">
                <div class="col-md-8">
                        <div class="card m-5">
                                <div class="card-header text-white mb-3"style="background:#e58c8a;">
                                        <h1>Paciente nº {{$patient->id}}</h1>
                                </div>
                                <ul class="list-group">
                                        <li class="list-group-item"><strong> DNI : </strong>
                                                {{ $patient->dni }}</li>
                                        <li class="list-group-item"><strong> Nombre : </strong>
                                                {{ $patient->nombre }}</li>
                                        <li class="list-group-item"><strong> Primer Apellido : </strong>
                                                {{ $patient->apellido1 }}</li>
                                        <li class="list-group-item"><strong> Segundo Apellido : </strong>
                                                {{ $patient->apellido2}}</li>
                                        <li class="list-group-item"><strong> Sexo : </strong>
                                                {{ $patient->sexo}}</li>
                                        <li class="list-group-item"><strong> Fecha de Nacimiento : </strong>
                                                {{ $patient->fechaNacimiento}}</li>
                                        <li class="list-group-item"><strong> Dirección : </strong>
                                                {{ $patient->direccion}}</li>
                                        <li class="list-group-item"><strong> Población : </strong>
                                                {{ $patient->poblacion}}</li>
                                        <li class="list-group-item"><strong> CP : </strong>
                                                {{ $patient->CP}}</li>
                                        <li class="list-group-item"><strong> Teléfono : </strong>
                                                {{ $patient->telefono }}</li>
                                </ul>


                        </div>
                </div>
        </div>
</div>


@endsection