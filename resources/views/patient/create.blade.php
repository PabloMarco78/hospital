@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
            <div class="class-header m-5">

                <h2 class="text-center"><i class="fas fa-address-card ml-5 mt-3"> Añadir nuevo paciente </i></h2>
            </div>
            </div>
                <div class="card-body">

                    <form action="/patients" method="post">
                        @csrf


                        <div class="form-row">
                           <!-- <div class="form-group col-md-6">
                                <label for="dni"><strong>Doctor</strong></label>
                                <input type="text" name="doctor_id" value="{{ old('doctor_id')}}" class="form-control" id="doctor_id" aria-describedby="H" placeholder="Introduce el id del doctor">
                                @error('doctor_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>-->
                            <div class="form-group col-md-6">
                                <label for="dni"><strong>DNI</strong></label>
                                <input type="text" name="dni" value="{{ old('dni')}}" class="form-control" id="dni" aria-describedby="H" placeholder="Introduce el dni">
                                @error('dni')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>


                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="nombre"><strong>Nombre</strong></label>
                                <input type="text" name="nombre" value="{{ old('nombre')}}" class="form-control" id="nombre" aria-describedby="H" placeholder="Introduce el nombre">
                                @error('nombre')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="apellido1"><strong>Primer Apellido</strong></label>
                                <input type="text" name="apellido1" value="{{ old('apellido1')}}" class="form-control" id="apellido1" aria-describedby="H" placeholder="Introduce el primer apellido">
                                @error('apellido1')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="apellido2"><strong>Segundo Apellido</strong></label>
                                <input type="text" name="apellido2" value="{{ old('apellido2')}}" class="form-control" id="apellido2" aria-describedby="H" placeholder="Introduce el segundo apellido">
                                @error('apellido2')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="sexo"><strong>Sexo</strong></label>
                                <input type="text" name="sexo" value="{{ old('sexo')}}" class="form-control" id="sexo" aria-describedby="H" placeholder="Introduce H / M">
                                @error('sexo')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                            </div>

                            <div class="form-group col-md-3">
                                <label for="fechaNacimiento"><strong>Fecha De Nacimiento</strong></label>
                                <input type="text" name="fechaNacimiento" value="{{ old('fechaNacimiento')}}" class="form-control" id="fechaNacimiento" aria-describedby="H" placeholder="formato aaaa-mm-dd">
                                @error('fechaNacimiento')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="direccion"><strong>Dirección</strong></label>
                                <input type="text" name="direccion" value="{{ old('direccion')}}" class="form-control" id="direccion" aria-describedby="H" placeholder="Introduce la dirección">
                                @error('direccion')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="poblacion"><strong>Población</strong></label>
                                <input type="text" name="poblacion" value="{{ old('poblacion')}}" class="form-control" id="poblacion" aria-describedby="H" placeholder="Introduce la población">
                                @error('poblacion')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="provincia"><strong>Provincia</strong></label>
                                <input type="text" name="provincia" value="{{ old('provincia')}}" class="form-control" id="provincia" aria-describedby="H" placeholder="Introduce la provincia">
                                @error('provincia')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="CP"><strong>CP</strong></label>
                                <input type="text" name="CP" value="{{ old('CP')}}" class="form-control" id="CP" aria-describedby="H" placeholder="Introduce el Código Postal">
                                @error('CP')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>




                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="telefono"><strong>Teléfono</strong></label>
                                <input type="text" name="telefono" value="{{ old('telefono')}}" class="form-control" id="telefono" aria-describedby="H" placeholder="Introduce el teléfono">
                                @error('telefono')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <button type="submit" class="btn text-white ml-3 mt-3" style="background:#e58c8a;">Añadir Paciente</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        @endsection