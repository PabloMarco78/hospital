@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center ">
        <div class="col-md-10">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
                <div class="class-header m-5">


                <h3 class="text-center"><i class="fas fa-user-edit"> Editar el perfil del paciente {{$patient->nombre}} {{$patient->apellido1}} </i></h3>
                </div>
            </div>
        

        <form action="/patients/{{$patient->id}}" method="post">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group row">
                <label for="dni" class="col-sm-2 col-form-label ml-5"><strong>DNI</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="dni" value="{{ old('dni') ?  old('dni') : $patient->dni }}" class="form-control">
                    @error('dni')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="nombre" class="col-sm-2 col-form-label ml-5"><strong>Nombre</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="nombre" value="{{ old('nombre') ?  old('nombre') : $patient->nombre }}" class="form-control">
                    @error('nombre')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="apellido1" class="col-sm-2 col-form-label ml-5"><strong>Primer Apellido</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="apellido1" value="{{ old('apellido1') ?  old('apellido1') : $patient->apellido1 }}" class="form-control">
                    @error('apellido1')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="apellido2" class="col-sm-2 col-form-label ml-5"><strong>Segundo Apellido</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="apellido2" value="{{ old('apellido2') ?  old('apellido2') : $patient->apellido2 }}" class="form-control">
                    @error('apellido2')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="sexo" class="col-sm-2 col-form-label ml-5"><strong>Sexo (H/M)</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="sexo" value="{{ old('sexo') ?  old('sexo') : $patient->sexo }}" class="form-control">
                    @error('sexo')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="fechaNacimiento" class="col-sm-2 col-form-label ml-5"><strong>Fecha de Nacimiento</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="fechaNacimiento" value="{{ old('fechaNacimiento') ?  old('fechaNacimiento') : $patient->fechaNacimiento }}" class="form-control">
                    @error('fechaNacimiento')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="direccion" class="col-sm-2 col-form-label ml-5"><strong>Dirección</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="direccion" value="{{ old('direccion') ?  old('direccion') : $patient->direccion }}" class="form-control">
                    @error('direccion')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="poblacion" class="col-sm-2 col-form-label ml-5"><strong>Población</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="poblacion" value="{{ old('poblacion') ?  old('poblacion') : $patient->poblacion }}" class="form-control">
                    @error('poblacion')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="provincia" class="col-sm-2 col-form-label ml-5"><strong>Provincia</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="provincia" value="{{ old('provincia') ?  old('provincia') : $patient->provincia }}" class="form-control">
                    @error('provincia')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="CP" class="col-sm-2 col-form-label ml-5"><strong>CP</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="CP" value="{{ old('CP') ?  old('CP') : $patient->CP }}" class="form-control">
                    @error('CP')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="telefono" class="col-sm-2 col-form-label ml-5"><strong>Teléfono</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="telefono" value="{{ old('telefono') ?  old('telefono') : $patient->telefono }}" class="form-control">
                    @error('telefono')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div>

                <button type="submit" class="btn text-white ml-5 mb-5" style="background:#e58c8a;">Editar</button>
            </div>
        </form>
    </div>
</div>
</div>
</div>
@endsection