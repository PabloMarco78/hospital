@extends('layouts.app')

@section('content')
<div class="container-fluid ">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
                <div class="class-header m-5">
                    <h3 class="text-center"><i class="fas fa-user-edit"> Editar el perfil del doctor: {{$doctor->nombre}} {{$doctor->apellido1}} </i></h3>
                </div>
            </div>
            <form action="/doctors/{{$doctor->id}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group row ">
                    <label for="dni" class="col-sm-2 col-form-label ml-5 "> <strong>DNI</strong> </label>
                    <div class="col-sm-10 ml-5">
                        <input type="text" class="form-control" name="dni" value="{{ old('dni') ?  old('dni') : $doctor->dni }}" class="form-control"">        
                            @error('dni')
                                    <div class=" alert alert-danger">{{ $message }}
                    </div>
                    @enderror
                </div>
        </div>

        <div class="form-group row">
            <label for="nombre" class="col-sm-2 col-form-label ml-5"> <strong>Nombre</strong> </label>
            <div class="col-sm-10 ml-5">
                <input type="text" class="form-control" name="nombre" value="{{ old('nombre') ?  old('nombre') : $doctor->nombre }}" class="form-control""> 
                            @error('nombre')
                                    <div class=" alert alert-danger">{{ $message }}
            </div>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="apellido1" class="col-sm-2 col-form-label ml-5"><strong>Primer Apellido</strong></label>
        <div class="col-sm-10 ml-5">
            <input type="text" class="form-control" name="apellido1" value="{{ old('apellido1') ?  old('apellido1') : $doctor->apellido1 }}" class="form-control""> 
                            @error('apellido1')
                                    <div class=" alert alert-danger">{{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="apellido2" class="col-sm-2 col-form-label ml-5"><strong>Segundo Apellido</strong></label>
    <div class="col-sm-10 ml-5">
        <input type="text" class="form-control" name="apellido2" value="{{ old('apellido2') ?  old('apellido2') : $doctor->apellido2 }}" class="form-control""> 
                            @error('apellido2')
                                    <div class=" alert alert-danger">{{ $message }}
    </div>
    @enderror
</div>
</div>

<div class="form-group row">
    <label for="telefono" class="col-sm-2 col-form-label ml-5"><strong>Teléfono</strong></label>
    <div class="col-sm-10 ml-5">
        <input type="text" class="form-control" name="telefono" value="{{ old('telefono') ?  old('telefono') : $doctor->telefono }}" class="form-control"">
                            @error('telefono')
                        <div class=" alert alert-danger">{{ $message }}
    </div>
    @enderror
</div>
</div>

<div class="form-group row">
    <label for="especialidad" class="col-sm-2 col-form-label ml-5"><strong>Especialidad</strong></label>
    <div class="col-sm-10 ml-5">
        <input type="text" class="form-control" name="especialidad" value="{{ old('especialidad') ?  old('especialidad') : $doctor->especialidad }}" class="form-control""> 
                            @error('especialidad')
                                    <div class=" alert alert-danger">{{ $message }}
    </div>
    @enderror
</div>
</div>

<div>
    <button type="submit" class="btn text-white  ml-5 mb-5" style="background:#e58c8a;">Editar</button>
</div>
</form>
</div>
</div>
</div>
</div>
@endsection