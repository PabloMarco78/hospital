@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center m-5">
        <div class="col-md-10" class="rounded">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">

                <div class="class-header text-center m-5">
                    <i class="fas fa-user-circle fa-3x float-right"></i>
                    <h3>Editar el perfil de {{$user->name}}</h3>
                </div>
            </div>
            <div class="card-body">


                <form action="/users/{{$user->id}}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label ml-5"><strong>Nombre</strong></label>
                        <div class="col-sm-10 ml-5">
                            <input type="text" name="name" value="{{ old('name') ?  old('name') : $user->name }}" class="form-control"">        
                                @error('name')
                                <div class=" alert alert-danger">{{ $message }}
                        </div>
                        @enderror
                    </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label ml-5"><strong>Email</strong></label>
                <div class="col-sm-10 ml-5">
                    <input type="text" name="email" value="{{ old('email') ?  old('email') : $user->email }}" class="form-control""> 
                                        @error('email')
                                        <div class=" alert alert-danger">{{ $message }}
                </div>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-sm-2 col-form-label ml-5"><strong>Contraseña</strong></label>
            <div class="col-sm-10 ml-5">
                <input type="text" name="password" value="{{ old('password') ?  old('password') : $user->password }}" class="form-control""> 
                                            @error('password')
                                            <div class=" alert alert-danger">{{ $message }}
            </div>
            @enderror
        </div>
    </div>

    <!--<div>
            <label for="role_id">Rol</label>
            <input type="text" name="role_id" value="{{ old('role_id') ?  old('role_id') : $user->role_id }}" class="form-control""> 
            @error('role_id')
            <div class=" alert alert-danger">{{ $message }}</div>
            @enderror
        </div>-->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-2 pt-0 ml-5"><strong>Asignar Roles</strong></legend>
            <div class="col-sm-10 ml-5">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="role_id" id="gridRadios1" value="1" checked>
                    <label class="form-check-label" for="gridRadios1">
                        Doctor
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="role_id" id="gridRadios2" value="2">
                    <label class="form-check-label" for="gridRadios2">
                        Paciente
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="role_id" id="gridRadios3" value="3">
                    <label class="form-check-label" for="gridRadios3">
                        Administrador
                    </label>
                </div>
            </div>
        </div>
    </fieldset>
    <button type="submit" class="btn text-white ml-5 mb-5" style="background:#e58c8a;">Editar</button>
</div>
</form>


</div>
</div>
</div>

</div>
@endsection