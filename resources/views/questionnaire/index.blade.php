@extends('layouts.app')

@section('content')
<div class="container-fluid mb-5">
    <div class="row justify-content-center">
        <div class="col-xs-6 col-sm-12 col-md-10 col-lg-12 ">
        <div class="card border-secondary m-5">
        <div class="card border-secondary m-5">
                <div class="class-header m-5">

           
                <h1 class="text-center"> ¡Muchas gracias, {{$questionnaire->name}}! </h1><br>
                
            </div>
        </div>

            <table class="table table-striped">
                <thead class="bg-dark text-white">
                    <tr style="text-align:center">
                        <th scope="col">Pregunta</th>
                        <th scope="col">Respuesta</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($questionnaire->responses as $response)
                    <tr>

                        <td style="text-align:center">{{ $response->question_id }} </td>
                        <td style="text-align:center">{{ $response->answer_id }} </td>
                        @endforeach

                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection