@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
                <div class="class-header m-5">
                    
                        <h3 class="text-center"> ¡ Bienvenida {{ Auth::user()->name  }} ! ya puedes completar el cuestionario : "{{$survey->title}}" </h3>
                    </div>
                </div>
                <form action="/questionnaires/{{$survey->id}}-{{Str::slug($survey->title)}}" method="post">

                    @csrf

                    @foreach($survey->questions as $key => $question)
                    <div class="card mt-5">
                        <div class="card-header"> <strong> Pregunta nº {{$key+1}} :</strong> {{$question->question}}</div>

                        <div class="card-body">

                            @error('responses.' . $key . '.answer_id')
                            {{$message}}
                            @enderror
                            <ul class="list-group">
                                @foreach($question->answers as $answer)

                                <label for="answer{{ $answer->id }}">
                                    <li class="list-group-item">

                                        <input type="radio" name="responses[{{ $key }}][answer_id]" id="answer{{ $answer->id }}" class="mr-2" value="{{ $answer->id }}">
                                        {{ $answer->answer }}

                                        <input type="hidden" name="responses[{{ $key }}][question_id]" value="{{$question->id}}">

                                    </li>
                                </label>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach

                    <div class="card=mt-4">
                    
                <div class="class-header m-5">
                        <div class="card-header ml-3"> <strong> DATOS PERSONALES : </strong></div>
                        <label class="m-1" for="name"> Introduce tu Nombre </label>
                        <input type="text ml-3" name="questionnaire[name]" class="form-control" id="name" aria-describedby="nameHelp" placeholder="{{ Auth::user()->name  }}">
                        <small id="name" class="form-text text-muted ml-3"></small>
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div>
                        <button class="btn btn-dark" type="submit">Enviar respuestas</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection