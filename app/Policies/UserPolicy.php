<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */


    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }
    public function view(User $user)
    {
        return $user->id != 2;
    }
}
