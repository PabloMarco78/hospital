<?php

namespace App\Exports;

use App\Models\QuestionnaireResponse;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithHeadings;


class QuestionnairesExport implements FromCollection,WithHeadings
{
    public function collection()
    {
        return QuestionnaireResponse::all();
    
    }
    public function headings(): array
    {
        return [
            'Id', 'questionnaire_id', 'question_id', 'answer_id', 'Created At', 'Updated At'
        ];
    }
}