<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//Nuestro cuestionario es una colección de preguntas
class Survey extends Model
{
    use HasFactory;

    protected $fillable =    [
        
        'user_id',
        'title',
        'description'

    ];

    public function user() {
        return $this->belongsTo(User::class);
      }
      //un cuestionario tiene muchas preguntas
      public function questions() {
        return $this->hasMany(Question::class);
      }

      public function questionnaires() {
        return $this->hasMany(Questionnaire::class);
      }
}
