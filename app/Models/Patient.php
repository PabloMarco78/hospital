<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;


    protected $fillable =
    [
        
        'dni',
        'nombre',
        'apellido1',
        'apellido2',
        'sexo',
        'fechaNacimiento',
        'direccion',
        'poblacion',
        'provincia',
        'CP',
        'telefono'        
    ];

    public function doctor()
        {
            return $this->belongsTo(Doctor::class);
        }       
}
