<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    use HasFactory;

    protected $fillable=[

        'survey_id',
        'name',

    ];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    } 
    
    public function responses()
    {
        return $this->hasMany(QuestionnaireResponse::class);
    }     
}
