<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //me va a pasar el listado de cuestionarios en función del usuario
        $surveys=Survey::all();
       // $surveys=auth()->user()->surveys;
        

        //dd($surveys);

        return view('home', compact('surveys'));
    }
}
