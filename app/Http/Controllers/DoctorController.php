<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Doctor;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $nombre = $request->nombre;
        $query = Doctor::query();
        

        if ($nombre) {
            $query->where('nombre', 'like',  "%$nombre%");
        }

       $doctors = $query->paginate(15);

       $doctors->withPath("/doctors?nombre=$nombre");

        return view('doctor.index', [
            'doctors' => $doctors,
            'nombre' => $nombre,
            
        ]);

       /* return view('doctor.index', ['doctors' => $doctors]);
        dd($doctors);
        return $doctors;*/
    }

    /**
     * Show the form for creating a new resource.
     * nbghyuiop
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->authorize('create', Doctor::class);
    
        return view('doctor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'dni' => 'required|unique:doctors|max:9',
            'nombre' => 'required|string|max:255',
            'apellido1' => 'required|string|max:255',
            'apellido2' => 'required|string|max:255',
            'telefono' => 'required|string|max:255',
            'especialidad' => 'required|string|max:255',
        ];

        $request->validate($rules);

        $doctor = Doctor::create($request->all());


        return redirect('/doctors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {

        return view('doctor.show', ['doctor' => $doctor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Doctor $doctor)
    {
        $this->authorize('update', $doctor);
        return view('doctor.edit', ['doctor' => $doctor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {

        $rules = [
            'dni' => 'required|max:9|unique:doctors,dni, ' . $doctor->id,        
            'nombre' => 'required|string|max:255',
            'apellido1' => 'required|string|max:255',
            'apellido2' => 'required|string|max:255',
            'telefono' => 'required|string|max:255',
            'especialidad' => 'required|string|max:255',
        ];

        $request->validate($rules);

        $doctor->fill($request->all());

        $doctor->save();
        return redirect('/doctors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        $this->authorize('delete', $doctor);
        $doctor->delete();
        return back();
    }
}
