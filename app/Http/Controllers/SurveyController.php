<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use Illuminate\Http\Request;
use App\Models\Survey;

class SurveyController extends Controller
{

    public function __construct()
    {
        //previene que alguien no logado acceda a este controller
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $request->title;
        $query = Survey::query();
        

        if ($title) {
            $query->where('title', 'like',  "%$title%");
        }
        $surveys = Survey::paginate(15);

        $surveys->withPath("/surveys?title=$title");

        return view('survey.index', [
            'surveys' => $surveys,
            'title' => $title,
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('survey.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
                        
        ];
        $request->validate($rules);
        $survey=auth()->user()->survey()->create($request->all());

        
       // $survey=Survey::create($request->all());
       
        

        return redirect('/surveys');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        
        //lazy loading(relación hasMany: un cuestionario tiene muchas preguntas y cada 
        //pregunta tiene 4 respuestas)
        $survey->load('questions.answers');

        //dd($survey);
    
        return view('survey.show', compact ('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        return view ('survey.edit', ['survey'=>$survey]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        $survey->fill($request->all());

        $survey->save();
        return redirect('/surveys');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        $survey->delete();
        return back();
    }
}
