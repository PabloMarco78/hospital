<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            
            'doctor_id' => $this->faker->numberBetween(1,10),
            'dni' => $this->faker->numberBetween(1,1000000),
            'nombre' => $this->faker->word(),
            'apellido1' => $this->faker->word(),
            'apellido2' => $this->faker->word(),
            'sexo' =>$this->faker->word('H','M'),
            'fechaNacimiento' =>$this->faker->date(),
            'direccion' =>$this->faker->word(),
            'poblacion' =>$this->faker->word(),
            'provincia' =>$this->faker->word(),
            'CP' =>$this->faker->numberBetween(1,100),
            'telefono' => $this->faker->numberBetween(1,100),
            
                ];
    }
}
