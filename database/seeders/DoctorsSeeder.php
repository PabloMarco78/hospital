<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Doctor;

class DoctorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Doctor::create([
            'dni' => '44618092Y',
            'nombre' => 'Cesareo',
            'apellido1' => 'Esteban',
            'apellido2' => 'Fiera',
            'telefono' => '622506984',
            'especialidad' => 'Oncologia',
        ]);
        Doctor::factory(20)->create();

    }
}
